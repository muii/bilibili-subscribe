package com.gitee.muii.bilibili.subscribe.entity

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonNames

@Serializable
@ExperimentalSerializationApi
data class DynamicEntity(val code: Int, val data: DynamicEntityData)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityData(@JsonNames("has_more") val hasMore: Boolean, val items: List<DynamicEntityItem>)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityItem(val type: String, val modules: DynamicEntityModules)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityModules(
    @JsonNames("module_dynamic") val moduleDynamic: DynamicEntityModuleDynamic,
    @JsonNames("module_author") val moduleAuthor: DynamicEntityModuleAuthor
)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityModuleAuthor(@JsonNames("pub_ts") val pubTs: Long)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityModuleDynamic(val major: DynamicEntityMajor = DynamicEntityMajor())

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityMajor(val archive: DynamicEntityArchive = DynamicEntityArchive("", ""))

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityArchive(val bvid: String, val title: String)

@Serializable
@ExperimentalSerializationApi
data class DynamicEntityCard(val desc: DynamicEntityCardDesc, val card: String)

@Serializable
data class DynamicEntityCardDesc(
    val timestamp: Long,
    val type: Long,
    @SerialName("dynamic_id") val dynamicId: Long,
    val bvid: String
)

@Serializable
@ExperimentalSerializationApi
data class Card(val desc: String, @JsonNames("short_link_v2") val shortLink: String, val title: String)
