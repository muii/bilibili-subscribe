package com.gitee.muii.bilibili.subscribe.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.redis.core.HashOperations
import org.springframework.data.redis.core.RedisTemplate
import org.springframework.data.redis.core.SetOperations
import org.springframework.data.redis.core.ValueOperations

@Configuration
class RedisConfig {

    @Autowired
    private lateinit var redisTemplate: RedisTemplate<String, String>

    @Bean
    fun valueOperations(): ValueOperations<String, String> {
        return redisTemplate.opsForValue()
    }

    @Bean
    fun setOperations(): SetOperations<String, String> {
        return redisTemplate.opsForSet()
    }

    @Bean
    fun hashOperations(): HashOperations<String, String, String> {
        return redisTemplate.opsForHash()
    }

}
