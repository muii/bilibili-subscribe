package com.gitee.muii.bilibili.subscribe.config

import com.gitee.muii.bilibili.subscribe.jobs.DynamicJob
import org.quartz.CronScheduleBuilder
import org.quartz.JobBuilder
import org.quartz.JobDetail
import org.quartz.Trigger
import org.quartz.TriggerBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class QuartzConfig(
    @Value("\${jobs.dynamic.cron:0 0/1 * * * ?}")
    val cron: String
) {

    @Bean
    fun dynamicJob(): JobDetail {
        return JobBuilder.newJob(DynamicJob::class.java)
            .withIdentity("dynamicJob", "default")
            .storeDurably()
            .build()
    }

    @Bean
    fun dynamicTrigger(): Trigger {
        return TriggerBuilder.newTrigger()
            .forJob(dynamicJob())
            .withIdentity("dynamicTrigger", "default")
            .startNow()
            .withSchedule(CronScheduleBuilder.cronSchedule(cron))
            .build()
    }

}
