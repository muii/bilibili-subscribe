package com.gitee.muii.bilibili.subscribe

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BilibiliSubscribeApplication

fun main(args: Array<String>) {
    runApplication<BilibiliSubscribeApplication>(*args)
}
