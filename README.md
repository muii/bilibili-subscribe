### 项目简介

#### 注意

本项目使用[木兰宽松许可证, 第2版](http://license.coscl.org.cn/MulanPSL2)(可点击链接查看),请遵守该协议.

#### 如何运行

##### Windows环境

1. 下载并配置Java

从[OepnJDK](https://jdk.java.net/archive/)(可点击此链接)网站下载17.x的Windows版本,如下

![image-20221028004308253](imgs/README/image-20221028004308253.png)

或其他厂商的OpenJDK,如[Dragonwell17](https://github.com/alibaba/dragonwell17/releases)(可点击此链接),如下,若网络不佳,可通过[此链接](https://dragonwell.oss-cn-shanghai.aliyuncs.com/17.0.4.0.4%2B8/Alibaba_Dragonwell_Standard_17.0.4.0.4%2B8_x64_windows.zip)下载

![image-20221028004548307](imgs/README/image-20221028004548307.png)

2. 配置环境变量(或搜索‘配置JAVA环境变量’)

   配置`系统变量`,在`Path`变量后增加`;`+上一步解压完成后的路径+`\bin`;

   `Windows10+`版本系统,可在`Path`变量中点击`新建`,直接选择上一步解压完成后的路径+`\bin`的文件夹

3. 下载代码

![image-20221028004736999](imgs/README/image-20221028004736999.png)

4. 下载[Redis](https://github.com/microsoftarchive/redis/releases/download/win-3.2.100/Redis-x64-3.2.100.msi)(可点击此链接)
5. 下载[AnotherRedisDesktopManager](https://gitee.com/qishibo/AnotherRedisDesktopManager/releases/download/v1.5.8/Another-Redis-Desktop-Manager.1.5.8.exe)(可点击此链接),安装后运行,点击`新建连接`,直接点击`确认`
6. `新增Key`,键名填写`uids`,类型选择`Hash`,建完后,修改第一行,`Key`填`up主uid`,`value`填`自定义昵称`,可继续添加新行
7. `新增Key`,键名填写`randomTemplate`,类型选择`Set`,建完后,修改第一行,`value`填`回复模板`(需包含两个`%s`,第一个将被替换为`up主自定义昵称`,第二个将被替换为间隔时间,如`亲爱的%s,您距离上次发动态已经过去%s了,生产队的驴桑都不敢这么歇`),可继续添加新行
8. `新增Key`,键名填写`cookie`,类型选择`String`,建完后,在右侧添加参数,参数获得方式为

> 打开Chrome浏览器(或其他浏览器极速模式),进入bilibili,登陆账号,打开任意动态(最好自己发一个动态),按F12,选择网络(Network)页签,在筛选器输入add,之后发送一个评论,在F12页面找到”https://api.bilibili.com/x/v2/reply/add”请求,点击该行,在“标头”页面,找到“cookie”,复制值即为此步骤的参数

9. `新增Key`,键名填写`csrf`,类型选择`String`,建完后,在右侧添加参数,参数获得方式为

> 打开Chrome浏览器(或其他浏览器极速模式),进入bilibili,登陆账号,打开任意动态(最好自己发一个动态),按F12,选择网络(Network)页签,在筛选器输入add,之后发送一个评论,在F12页面找到”https://api.bilibili.com/x/v2/reply/add”请求,点击该行,在“负载”页面,找到“csrf”,复制值即为此步骤的参数

10. 解压代码,进入代码文件夹,按住`Shift`和鼠标右键,选择`在终端打开`或`在此处打开PowerShell窗口`(之后在该窗口执行`start cmd`,接下来在cmd窗口执行),执行`gradlew build`
11. 完成之后,继续执行`gradlew clean bootJar`
12. 进入`build\libs`文件夹
13. 打开终端或CMD,执行`java -Dspring.profiles.active=prod -jar bilibili-subscribe-0.0.1-SNAPSHOT.jar`(jar文件名替换为本文件夹内的文件)

##### Linux环境大概只有程序员才使用吧,自行摸索,Java需使用17.x版本

